<?php

namespace Database\Factories;

use App\Models\Travel;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Tour>
 */
class TourFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $today = date('Y-m-d');

        // Mendapatkan angka acak antara 1 dan 30 (misalnya)
        $random_days = rand(1, 30);

        return [
            'travel_id' => Travel::all()->random()->id,            
            'name' => fake()->text(20),
            'starting_date' => $today,
            'ending_date' => date('Y-m-d', strtotime($today . ' +' . $random_days . ' days')),
            'price' => fake()->randomFloat(2, 10, 999),
        ];
    }
}
